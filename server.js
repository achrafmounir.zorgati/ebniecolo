const express = require('express');
const connectDB = require('./config/db')

const app = express();

connectDB();


app.use(express.json({ extended: false}));



if(process.env.NODE_ENV === 'production'){
    //set static folder
    app.use(express.static('admin/build'))
    app.get('*',(req,res)=> {
        res.sendFile(path.resolve(__dirname,'admin','build','index.html'))
    })
}


const PORT =  process.env.PORT || 5000;

app.listen(PORT,()=> console.log(`Server started on port ${PORT}`));